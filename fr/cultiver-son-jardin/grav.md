# Installation de Grav

## Introduction

Grav est le logiciel utilisé pour créer des sites sur [frama.site](https://frama.site/).

<p class="alert alert-info">
  <b>Ce tutoriel est en cours de rédaction</b>.
  <br />
  Dans la suite de ce tutoriel, les instructions seront données pour un serveur dédié sous Debian Stretch, php 7.0 et un serveur web Nginx.
</p>

## Pré-requis

* PHP 5.5.9 ou supérieur. Normalement, les [modules suivants](https://learn.getgrav.org/basics/requirements#php-requirements) sont déjà installés sur votre serveur.
* git

`$ apt install php7.0 php7.0-gd php7.0-mbstring php7.0-curl php7.0-xml php7.0-zip git`

## Installation

### Semer

Clonez le dépôt de Grav :

```
$ cd /var/www/html
$ git clone https://github.com/getgrav/grav.git
$ cd grav
$ bin/grav install
$ cd /var/www/html
$ chown -R www-data grav
```
